/*
Navicat MySQL Data Transfer

Source Server         : Local
Source Server Version : 50711
Source Host           : localhost:3306
Source Database       : fantasy-world

Target Server Type    : MYSQL
Target Server Version : 50711
File Encoding         : 65001

Date: 2016-04-11 23:56:04
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `No` int(11) NOT NULL,
  `UserName` varchar(255) NOT NULL,
  `LoginName` varchar(255) NOT NULL,
  `LoginPassword` varchar(255) DEFAULT NULL,
  `LastLoginTime` timestamp NULL DEFAULT NULL,
  `LastLoginDevice` varchar(255) DEFAULT NULL,
  `Level` int(11) DEFAULT NULL,
  `HomeCity` varchar(255) NOT NULL,
  `RegistrationTime` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `VIPLevel` int(11) DEFAULT NULL,
  `Money` int(11) DEFAULT NULL,
  PRIMARY KEY (`No`,`UserName`,`HomeCity`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
