package com.http.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.database.service.TestService;
import com.database.service.UserTableService;
import com.database.service.impl.UserTableServiceImpl;
import com.database.table.UserModel;
 
/**
 * 
 * @author Administrator
 *
 */
@Controller
@RequestMapping(value = "/Login")
public class Login { 
	 
	@Autowired
	private TestService testService;
	
	@Autowired
	private UserTableService UserService;
	
	/*
	 * 跳转test
	 */
	@RequestMapping(value="/toLogin",method = {RequestMethod.POST})
	public @ResponseBody
	String toLogin(HttpServletRequest request, HttpServletResponse response){
		String strUsername = request.getParameter("username");
		String strPassword = request.getParameter("password");
		System.out.println("username:"+strUsername+"    password:"+strPassword);
		if(strUsername.equals("a") && strPassword.equals("a")){
			return "yes";			
		}
		return "abcdefg";
	}
	
	@RequestMapping(value="/toMgmtLogin",method = {RequestMethod.GET})
	public @ResponseBody
	String toMgmtLogin(Model model){	
		UserModel User = new UserModel();
		if(UserService.Login(User, "123", "123")){
			return "Yes";
		}
		return "No";
	}
	/*
	 * 跳转test
	 */
	@RequestMapping(value="/testHibernate",method = {RequestMethod.GET})
	public String testHibernate(){
		List<Object[]> objects = testService.test();
		if(objects!=null&&objects.size()>0)
			System.out.println(objects.get(0));
		return "test";
	}
	
}
