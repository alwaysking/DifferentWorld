package com.websocket.service;

import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.jetty.websocket.api.Session;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketClose;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketConnect;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketError;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketMessage;
import org.eclipse.jetty.websocket.api.annotations.WebSocket;

@WebSocket
public class testWebSocket {
	
	static Map<Integer, Session> SessionMap = Collections.synchronizedMap(new HashMap<Integer, Session>());
	static int Id = 0;
	int Myid;
	
    @OnWebSocketClose
    public void onClose(int statusCode, String reason) {
        System.out.println("Close: " + reason);
        SessionMap.remove(Myid);
    }

    @OnWebSocketError
    public void onError(Throwable t) {
        System.out.println("Error: " + t.getMessage());
        SessionMap.remove(Myid);
    }

    @OnWebSocketConnect
    public void onConnect(Session session) {
        System.out.println("Connect: " + session.getRemoteAddress().getAddress());
        
        try {
            session.getRemote().sendString("Hello Webbrowser");
        } catch (IOException e) {
            System.out.println("IO Exception");
        }
        SessionMap.put(Id, session);
        Myid = Id;
        Id++;
    }

    @OnWebSocketMessage
    public void onMessage(String message) {
    	for (Session value : SessionMap.values()) {    		  
    		 try {
    			 value.getRemote().sendString(message);
    	        } catch (IOException e) {
    	            System.out.println("IO Exception");
    	        }  	  
    	}  
    }
}