package com.database.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.database.dao.BaseDao;
import com.database.service.TestService;
import com.database.service.UserTableService;
import com.database.table.UserModel;

@Component 
@Transactional
public class UserTableServiceImpl  implements UserTableService{

	@Resource
	private BaseDao baseDao;
	
	@Override
	public boolean Login(UserModel User, String LoginName, String LoginPassword){
		boolean rv = false;
		List<Object[]> list = baseDao.findBySql("select 'User'");
		if(list.size() != 0){
			rv = true;
		}		
		return rv;
	}
}
