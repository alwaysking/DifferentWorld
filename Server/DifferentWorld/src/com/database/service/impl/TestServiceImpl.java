package com.database.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.database.dao.BaseDao;
import com.database.service.TestService;

@Component 
@Transactional
public class TestServiceImpl implements TestService{
	
	@Resource
	private BaseDao baseDao;
	
	@Override
	public List<Object[]> test() {
		// TODO Auto-generated method stub
		List<Object[]> list = baseDao.findBySql("select 'helloworld' ");
		return list;
	}

}
