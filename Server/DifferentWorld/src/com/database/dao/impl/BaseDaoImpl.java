package com.database.dao.impl;

import java.io.Serializable;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.orm.hibernate4.SessionFactoryUtils;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.database.dao.BaseDao;

@Component
@Transactional
public class BaseDaoImpl implements BaseDao {
	/**
	 * 依赖注入
	 */ 
	@Resource
	private SessionFactory sessionFactory;  
	
	private Session getSession(){
		return sessionFactory.getCurrentSession();
	} 
	
	private Query createQuery(Session session, String hql, final Object[] args) {
		Query query = session.createQuery(hql);
		if (args != null && args.length > 0) {
			for (int i = 0; i < args.length; i++) {
				query.setParameter(i, args[i]);
			}
		}
		return query;
	}
	
	private SQLQuery createSQLQuery(Session session, String sql, Object[] args) {
		SQLQuery query = session.createSQLQuery(sql);
		if (args != null && args.length > 0) {
			for (int i = 0; i < args.length; i++) {
				query.setParameter(i, args[i]);
			}
		}
		return query;
	}

	@Override
	public <T> void add(T t) {
		try {
			getSession().save(t);  
		} catch (Exception e) {
			throw e;
		}		
	} 

	@Override
	public <T> void update(T t) {
		try {
			getSession().update(t);
		} catch (Exception e) {
			throw e;
		}
	}

	@Override
	public <T> void deleteById(T t) {
		try {
			getSession().delete(t);
		} catch (Exception e) {
			throw e;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public <T> T findById(Class<T> clazz, Serializable id) {
		try {
			return (T) getSession().get(clazz, id);
		} catch (Exception e) { 
			throw e;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public <T> List<T> findByHql(String hql) { 
		try {
			return getSession().createQuery(hql).list();
		} catch (Exception e) {
			throw e;
		}  
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public <T> List<T> findByHql(String hql, Object[] args) {
		// TODO Auto-generated method stub		
		try {
			Query query = createQuery(getSession(), hql, args);
			return query.list();	
		} catch (Exception e) {
			throw e;
		}  
	} 

	@SuppressWarnings("unchecked")
	@Override
	public List<Object[]> findBySql(String sql) {
		try { 
			return getSession().createSQLQuery(sql).list(); 
		} catch (Exception e) {
			throw e;
		}  
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Object[]> findBySql(String sql, Object[] args) {
		// TODO Auto-generated method stub
		try {
			Query query = createSQLQuery(getSession(), sql, args);
			return query.list();			
		} catch (Exception e) {
			throw e;
		}
	} 
 
	@Override
	public void executeHQL(String hql) {
		// TODO Auto-generated method stub
		try {
			executeHQL(hql, null);	
		} catch (Exception e) {
			throw e;
		}	 
	}

	@Override
	public void executeHQL(String hql, Object args) {
		// TODO Auto-generated method stub
		try {
			Query query=getSession().createQuery(hql);  
			query.setParameter(0,args);
			query.executeUpdate();
		} catch (Exception e) {
			throw e;
		}
		
	}

	@Override
	public void executeHQL(String hql, Object[] args) {
		// TODO Auto-generated method stub
		try {
			Query query=getSession().createQuery(hql);  
			for(int i=0;i<args.length;i++)
		    {
				query.setParameter(i,args[i]);
		    }
			query.executeUpdate();			
		} catch (Exception e) {
			throw e;
		}
	}

	@Override
	public void executeSQL(String sql) {
		// TODO Auto-generated method stub
		try {
			executeSQL(sql, null);			
		} catch (Exception e) {
			throw e;
		}
	}

	@Override
	public void executeSQL(String sql, Object args) {
		// TODO Auto-generated method stub 
		try {
			Query query=getSession().createSQLQuery(sql);  
			query.setParameter(0,args);
			query.executeUpdate();
		} catch (Exception e) {
			throw e;
		}
		
	}

	@Override
	public void executeSQL(String sql, Object[] args) {
		// TODO Auto-generated method stub  
		try {
			Query query=getSession().createSQLQuery(sql);  
			for(int i=0;i<args.length;i++)
		    {
				query.setParameter(i,args[i]);
		    }
			query.executeUpdate();
		} catch (Exception e) {
			throw e;
		} 
	}

	@Override
	public List<Object[]> findByProcedure(String sql, String[] args) {
		// TODO Auto-generated method stub
		//方法一
		List<Object[]> list = new ArrayList<Object[]>();  
	  
		ResultSet rs = null;  
		Connection conn = null;  
		CallableStatement proc = null;   
		try {   
			conn = SessionFactoryUtils.getDataSource(sessionFactory).getConnection();  
			proc = conn.prepareCall(sql); 
			if(args!=null && args.length>0){
				for(int i=1 ; i<=args.length ; i++){ 
					proc.setString(i, args[i-1]);
				}
			} 
//					proc.registerOutParameter(3, Types.VARCHAR);  
			proc.execute();  
//					System.out.println(proc.getString(3));
			Object[] item = null;    
			rs = (ResultSet) proc.getResultSet();
			ResultSetMetaData resultSetMetaData = rs.getMetaData();
			int count = resultSetMetaData.getColumnCount(); 
			while (rs != null && rs.next()) {  
				item = new  Object[count];   
				for(int i=0;i<count;i++){
					item[i] =  rs.getObject(i+1);
				} 
				list.add(item); 
			}     
		} catch (Exception e) {    
			e.printStackTrace();   
			throw new RuntimeException(e.getMessage());  
		} finally {  
			try {  
				if (null != rs) {  
					rs.close();  
					if (null != proc) {  
						proc.close();  
					}  
					if (null != conn) {  
						conn.close();  
					}  
				}  
			} catch (Exception ex) {  
  
			}  
		}   
		//方法二
//		try {
//			SQLQuery query = getSession1().createSQLQuery(sql); 
//			if(args!=null && args.length>0){
//				for(int i=0 ; i<args.length ; i++){ 
//					query.setString(i, args[i]);
//				}
//			}
//			list =query.list();
//			for(int i =0;i<list.size();i++){  
//				System.out.println(list.get(i));  
//			}  
//		} catch (Exception e) {
//			throw runtimeException(e);
//		}		
		return list;
	}
	
}
