package com.database.dao;

import java.io.Serializable;
import java.util.List;

public interface BaseDao {
	   /**
	    * 新增
	    * @param t
	    */
	   public <T> void add(T t);
	   /**
	    * 更新
	    * @param t
	    */
	   public <T> void update(T t);
	   /**
	    * 删除
	    * @param t
	    */
	   public <T> void deleteById(T t);
	   /**
	    * 查询对象ById
	    */
	   public <T> T findById(Class<T> clazz,Serializable id);
	   /**
	    * hql 查询列表
	    */
	   public <T> List<T> findByHql(String hql); 
	   /**
	    * hql 查询列表
	    */
	   public <T> List<T> findByHql(String hql, Object[] args); 
	   /**
	    * sql 查询列表
	    */
	   public List<Object[]> findBySql(String sql);
	   /**
	    * sql 查询列表
	    */
	   public List<Object[]> findBySql(String sql, Object[] args);
	  
	   public void executeHQL(String hql);

	   public void executeHQL(String hql, Object args);
	   
	   public void executeHQL(String hql, Object[] args);

	   public void executeSQL(String sql);

	   public void executeSQL(String sql, Object args);

	   public void executeSQL(String sql, Object[] args);
	   
	   //调用带参的存储过程
	   public List<Object[]> findByProcedure(String sql, String[] args); 
}
