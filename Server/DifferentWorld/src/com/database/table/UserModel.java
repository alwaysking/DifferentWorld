package com.database.table;


import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "User")
public class UserModel {

	@Id
	@GeneratedValue(generator = "SYS_USERROLE_id_generator")
	@GenericGenerator(name = "SYS_USERROLE_id_generator", strategy = "uuid")
	private int No;
	
	@Column(name="UserName")
	private String UserName;
	
	@Column(name="LoginName")
	private String LoginName;
	
	@Column(name="LoginPassword")
	private String LoginPassword;
	
	@Column(name="LastLoginTime")
	private Timestamp LastLoginTime;
	
	@Column(name="LastLoginDevice")
	private String LastLoginDevice;
	
	@Column(name="Level")
	private int Level;
	
	@Column(name="HomeCity")
	private String HomeCity;
	
	@Column(name="RegistrationTime")
	private Timestamp RegistrationTime;
	
	@Column(name="VIPLevel")
	private int VIPLevel;
	
	@Column(name="Money")
	private int Money;

	public int getNo() {
		return No;
	}

	public void setNo(int no) {
		No = no;
	}

	public String getUserName() {
		return UserName;
	}

	public void setUserName(String userName) {
		UserName = userName;
	}

	public String getLoginName() {
		return LoginName;
	}

	public void setLoginName(String loginName) {
		LoginName = loginName;
	}

	public String getLoginPassword() {
		return LoginPassword;
	}

	public void setLoginPassword(String loginPassword) {
		LoginPassword = loginPassword;
	}

	public Timestamp getLastLoginTime() {
		return LastLoginTime;
	}

	public void setLastLoginTime(Timestamp lastLoginTime) {
		LastLoginTime = lastLoginTime;
	}

	public String getLastLoginDevice() {
		return LastLoginDevice;
	}

	public void setLastLoginDevice(String lastLoginDevice) {
		LastLoginDevice = lastLoginDevice;
	}

	public int getLevel() {
		return Level;
	}

	public void setLevel(int level) {
		Level = level;
	}

	public String getHomeCity() {
		return HomeCity;
	}

	public void setHomeCity(String homeCity) {
		HomeCity = homeCity;
	}

	public Timestamp getRegistrationTime() {
		return RegistrationTime;
	}

	public void setRegistrationTime(Timestamp registrationTime) {
		RegistrationTime = registrationTime;
	}

	public int getVIPLevel() {
		return VIPLevel;
	}

	public void setVIPLevel(int vIPLevel) {
		VIPLevel = vIPLevel;
	}

	public int getMoney() {
		return Money;
	}

	public void setMoney(int money) {
		Money = money;
	}
	
}
