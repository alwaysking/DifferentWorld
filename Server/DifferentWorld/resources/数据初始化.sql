/*
Navicat MySQL Data Transfer

Source Server         : mysqlLocal
Source Server Version : 50624
Source Host           : localhost:3306
Source Database       : helloworld

Target Server Type    : MYSQL
Target Server Version : 50624
File Encoding         : 65001

Date: 2016-02-04 14:53:54
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for sys_button
-- ----------------------------
DROP TABLE IF EXISTS `FW_User`;
CREATE TABLE `FW_User` (
  `ID` varchar(255) NOT NULL COMMENT '唯一标示',
  `UserName` varchar(255) NOT NULL COMMENT '用户名',
  `PassWord` varchar(255) NOT NULL COMMENT '密码',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户表';

-- ----------------------------
-- Records of sys_button
-- ----------------------------
INSERT INTO `FW_User` VALUES ('1', 'AlwaysKing', '12341234');

