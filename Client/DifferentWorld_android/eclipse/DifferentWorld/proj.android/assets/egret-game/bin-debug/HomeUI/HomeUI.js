// TypeScript file
var CHomeUI = (function (_super) {
    __extends(CHomeUI, _super);
    function CHomeUI() {
        _super.call(this);
        this.addEventListener(egret.Event.ADDED_TO_STAGE, this.onAddToStage, this);
    }
    var d = __define,c=CHomeUI,p=c.prototype;
    p.onAddToStage = function (event) {
        var button = new eui.Button();
        button.label = "Home";
        button.horizontalCenter = 0;
        button.verticalCenter = 0;
        this.addChild(button);
    };
    return CHomeUI;
}(egret.DisplayObjectContainer));
egret.registerClass(CHomeUI,'CHomeUI');
//# sourceMappingURL=HomeUI.js.map