// TypeScript file
var CMapUI = (function (_super) {
    __extends(CMapUI, _super);
    function CMapUI() {
        _super.call(this);
        this.addEventListener(egret.Event.ADDED_TO_STAGE, this.onAddToStage, this);
    }
    var d = __define,c=CMapUI,p=c.prototype;
    p.onAddToStage = function (event) {
        var button = new eui.Button();
        button.label = "Map";
        button.horizontalCenter = 0;
        button.verticalCenter = 0;
        this.addChild(button);
        button.addEventListener(egret.TouchEvent.TOUCH_TAP, this.back, this);
        //显示信息的label
        this.label = new egret.TextField();
        this.addChild(this.label);
        this.label.size = 20;
        this.label.fontFamily = "楷体";
        this.label.text = "还未获取到信息2";
        this.label.x = 100;
        this.gps = new egret.Geolocation();
        //监听经纬度变化的事件
        this.gps.addEventListener(egret.Event.CHANGE, this.onGotLocation, this);
        this.gps.addEventListener(egret.GeolocationEvent.UNAVAILABLE, this.unAvailable, this);
        this.gps.once(egret.GeolocationEvent.PERMISSION_DENIED, this.userDenied, this);
        //开始监听变化
        this.gps.start();
    };
    p.userDenied = function (e) {
        this.label.text = "用户拒绝访问位置信息，获取位置信息失败";
    };
    // 点击登录
    p.back = function (e) {
        // 事件
        var SwitchEvent = new CUISwitchEvent(CUISwitchEvent.Event);
        SwitchEvent.From = "MapUI";
        SwitchEvent.To = "LoginUI";
        //发送要求事件
        this.dispatchEvent(SwitchEvent);
    };
    p.onGotLocation = function (e) {
        this.label.text = "纬度: " + e.latitude.toFixed(4) +
            " 海拔: " + e.altitude +
            "n经度:" + e.longitude.toFixed(4)
            + " 速度: " + e.speed;
        //this.label.anchorOffsetX = this.label.width / 2;
    };
    p.unAvailable = function (e) {
        this.label.text = "获取位置信息失败: " + e.errorMessage + "n"
            + "错误类型: " + e.errorType;
        // this.label.anchorOffsetX = this.label.width / 2;
    };
    return CMapUI;
}(egret.DisplayObjectContainer));
egret.registerClass(CMapUI,'CMapUI');
//# sourceMappingURL=MapUI.js.map