// TypeScript file
// TypeScript file
var CNetEvent = (function (_super) {
    __extends(CNetEvent, _super);
    function CNetEvent(type, bubbles, cancelable) {
        if (bubbles === void 0) { bubbles = false; }
        if (cancelable === void 0) { cancelable = false; }
        _super.call(this, type, bubbles, cancelable);
        this.Finish = false;
        this.Context = null;
    }
    var d = __define,c=CNetEvent,p=c.prototype;
    CNetEvent.Event = "Callback";
    return CNetEvent;
}(egret.Event));
egret.registerClass(CNetEvent,'CNetEvent');
//# sourceMappingURL=NetEvent.js.map