// TypeScript file
var CUISwitchEvent = (function (_super) {
    __extends(CUISwitchEvent, _super);
    function CUISwitchEvent(type, bubbles, cancelable) {
        if (bubbles === void 0) { bubbles = false; }
        if (cancelable === void 0) { cancelable = false; }
        _super.call(this, type, bubbles, cancelable);
        this.From = "UISwitch";
        this.To = "UISwitch";
    }
    var d = __define,c=CUISwitchEvent,p=c.prototype;
    CUISwitchEvent.Event = "UISwitch";
    return CUISwitchEvent;
}(egret.Event));
egret.registerClass(CUISwitchEvent,'CUISwitchEvent');
//# sourceMappingURL=UISwitchEvent.js.map