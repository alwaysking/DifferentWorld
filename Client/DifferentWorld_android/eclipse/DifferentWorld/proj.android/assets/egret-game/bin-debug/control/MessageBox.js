// TypeScript file
/**
 * CMessageBox
 */
var CMessageBox = (function (_super) {
    __extends(CMessageBox, _super);
    // 构造函数
    function CMessageBox() {
        _super.call(this);
        // 设置皮肤
        this.skinName = "resource/Control/MessageBox.exml";
    }
    var d = __define,c=CMessageBox,p=c.prototype;
    // 内容展示函数
    p.Show = function (Stage, Title, Txt, Type) {
        var rv = CMessageBox.IDCNACEL;
        // 设置展示内容
        this.title = Title;
        this.txtContext.text = Txt;
        // 移动位置
        this.x = (Stage.width - this.width) / 2;
        this.y = (Stage.height - this.height) / 2;
        Stage.addChild(this);
        return rv;
    };
    // 返回的结果
    CMessageBox.IDOK = 0;
    CMessageBox.IDCNACEL = 1;
    // 设置样式
    CMessageBox.ID_OKCANCEL = 0;
    CMessageBox.ID_ClOSE = 1;
    return CMessageBox;
}(eui.Panel));
egret.registerClass(CMessageBox,'CMessageBox');
//# sourceMappingURL=MessageBox.js.map