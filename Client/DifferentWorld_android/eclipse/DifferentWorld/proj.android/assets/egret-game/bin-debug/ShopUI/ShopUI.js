// TypeScript file
var CShopUI = (function (_super) {
    __extends(CShopUI, _super);
    function CShopUI() {
        _super.call(this);
        this.addEventListener(egret.Event.ADDED_TO_STAGE, this.onAddToStage, this);
    }
    var d = __define,c=CShopUI,p=c.prototype;
    p.onAddToStage = function (event) {
        var button = new eui.Button();
        button.label = "Shop";
        button.horizontalCenter = 0;
        button.verticalCenter = 0;
        this.addChild(button);
    };
    return CShopUI;
}(egret.DisplayObjectContainer));
egret.registerClass(CShopUI,'CShopUI');
//# sourceMappingURL=ShopUI.js.map