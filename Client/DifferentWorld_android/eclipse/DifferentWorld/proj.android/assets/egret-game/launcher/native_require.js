
var game_file_list = [
    //以下为自动修改，请勿修改
    //----auto game_file_list start----
	"libs/modules/egret/egret.js",
	"libs/modules/egret/egret.native.js",
	"libs/modules/game/game.js",
	"libs/modules/game/game.native.js",
	"libs/modules/tween/tween.js",
	"libs/modules/socket/socket.js",
	"libs/modules/eui/eui.js",
	"libs/modules/res/res.js",
	"libs/modules/Mylocation/Mylocation.js",
	"bin-debug/communication/MyPostSocket.js",
	"bin-debug/control/LoadResUI.js",
	"bin-debug/control/MessageBox.js",
	"bin-debug/control/WaitingUI.js",
	"bin-debug/FightUI/HomeUI.js",
	"bin-debug/Help/define.js",
	"bin-debug/Help/Event/NetEvent.js",
	"bin-debug/Help/Event/UISwitchEvent.js",
	"bin-debug/Help/sys/AssetAdapter.js",
	"bin-debug/Help/sys/ThemeAdapter.js",
	"bin-debug/HomeUI/HomeUI.js",
	"bin-debug/LgoinUI/LoginUI.js",
	"bin-debug/Main.js",
	"bin-debug/MapUI/MapUI.js",
	"bin-debug/ShopUI/ShopUI.js",
	//----auto game_file_list end----
];

var window = this;

egret_native.setSearchPaths([""]);

egret_native.requireFiles = function () {
    for (var key in game_file_list) {
        var src = game_file_list[key];
        require(src);
    }
};

egret_native.egretInit = function () {
    egret_native.requireFiles();
    egret.TextField.default_fontFamily = "/system/fonts/DroidSansFallback.ttf";
    //egret.dom为空实现
    egret.dom = {};
    egret.dom.drawAsCanvas = function () {
    };
};

egret_native.egretStart = function () {
    var option = {
        //以下为自动修改，请勿修改
        //----auto option start----
		entryClassName: "Main",
		frameRate: 30,
		scaleMode: "showAll",
		contentWidth: 1280,
		contentHeight: 720,
		showPaintRect: false,
		showFPS: false,
		fpsStyles: "x:0,y:0,size:12,textColor:0xffffff,bgAlpha:0.9",
		showLog: false,
		logFilter: "",
		maxTouches: 2,
		textureScaleFactor: 1
		//----auto option end----
    };

    egret.native.NativePlayer.option = option;
    egret.runEgret();
    egret_native.Label.createLabel(egret.TextField.default_fontFamily, 20, "", 0);
    egret_native.EGTView.preSetOffScreenBufferEnable(true);
};