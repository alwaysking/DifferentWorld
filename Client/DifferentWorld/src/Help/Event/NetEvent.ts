// TypeScript file
// TypeScript file
class CNetEvent extends egret.Event
{
    public static Event:string = "Callback";
    public Finish:boolean = false;
    public Context:egret.HttpRequest = null;
    public constructor(type:string, bubbles:boolean=false, cancelable:boolean=false)
    {
        super(type,bubbles,cancelable);
    }
}