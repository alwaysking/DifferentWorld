// TypeScript file
class CUISwitchEvent extends egret.Event
{
    public static Event:string = "UISwitch";
    public From:string = "UISwitch";
    public To:string = "UISwitch";
    public constructor(type:string, bubbles:boolean=false, cancelable:boolean=false)
    {
        super(type,bubbles,cancelable);
    }
}