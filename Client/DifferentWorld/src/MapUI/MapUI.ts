// TypeScript file

class CMapUI extends egret.DisplayObjectContainer{
    public constructor(){
        super();
        this.addEventListener(egret.Event.ADDED_TO_STAGE,this.onAddToStage,this);
    }

    private onAddToStage(event:egret.Event){
       var button = new eui.Button();
        button.label = "Map";
        button.horizontalCenter = 0;
	    button.verticalCenter = 0;
	    this.addChild(button);
        button.addEventListener(egret.TouchEvent.TOUCH_TAP,this.back,this);       
    }

     // 点击登录
    private back(e: egret.TouchEvent){
        // 事件
        var SwitchEvent:CUISwitchEvent = new CUISwitchEvent(CUISwitchEvent.Event);
        SwitchEvent.From = "MapUI";
        SwitchEvent.To = "LoginUI";
        //发送要求事件
        this.dispatchEvent(SwitchEvent);
    }
}