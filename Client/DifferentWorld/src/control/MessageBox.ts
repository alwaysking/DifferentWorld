// TypeScript file
/**
 * CMessageBox
 */
class CMessageBox extends eui.Panel {
    // 返回的结果
    public static IDOK:number = 0;
    public static IDCNACEL:number = 1;
    // 设置样式
    public static ID_OKCANCEL:number = 0;
    public static ID_ClOSE:number = 1;
    // 绑定的控件
    public txtContext: eui.TextInput;     // 声明与ID相同名称的变量就可以动态绑定了

    // 构造函数
    public constructor() 
    {
        super();
        // 设置皮肤
        this.skinName = "resource/Control/MessageBox.exml";
    }

    // 内容展示函数
    public Show(Stage:egret.Stage,Title:string, Txt:string, Type:number):number
    {
        var rv:number = CMessageBox.IDCNACEL;

        // 设置展示内容
        this.title = Title;
        this.txtContext.text = Txt;

        // 移动位置
        this.x = (Stage.width - this.width)/2;
        this.y = (Stage.height - this.height)/2;

        Stage.addChild(this);
        return rv;
    }

}