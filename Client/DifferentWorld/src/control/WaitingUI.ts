// TypeScript file
//////////////////////////////////////////////////////////////////////////////////////
//
//  Copyright (c) 2014-2015, Egret Technology Inc.
//  All rights reserved.
//  Redistribution and use in source and binary forms, with or without
//  modification, are permitted provided that the following conditions are met:
//
//     * Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright
//       notice, this list of conditions and the following disclaimer in the
//       documentation and/or other materials provided with the distribution.
//     * Neither the name of the Egret nor the
//       names of its contributors may be used to endorse or promote products
//       derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY EGRET AND CONTRIBUTORS "AS IS" AND ANY EXPRESS
//  OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
//  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
//  IN NO EVENT SHALL EGRET AND CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
//  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
//  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;LOSS OF USE, DATA,
//  OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
//  LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
//  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
//////////////////////////////////////////////////////////////////////////////////////

class CWaitingUI extends egret.Sprite {

    public Lognmc: egret.MovieClip;                // Logo动画
    public MCF: egret.MovieClipDataFactory;        // 动画工厂类
    public BackGround:egret.Shape;                 // 半透明遮罩背景

    public constructor() {
        super();
         // 获取资源
        var data = RES.getRes("Waiting_json");
        var tex = RES.getRes("Waiting_png");

        // 初始化动画工厂类
        this.MCF = new egret.MovieClipDataFactory(data,tex);

        // 从工厂类中获取指定动画
        this.Lognmc = new egret.MovieClip(this.MCF.generateMovieClipData("Waiting"));
        this.Lognmc.x = (1280 - 200) / 2;
        this.Lognmc.y = (720 - 200) / 2;

        // 创建遮罩背景
        this.BackGround = new egret.Shape();
        this.BackGround.graphics.beginFill(0x8d8c8e,0.8);
        this.BackGround.graphics.drawRect( 0, 0, 1280, 720 );
        this.BackGround.graphics.endFill();
    }

    // 启动其他动态元素的显示
    public Show(): void
    {
        // 显示画面
        this.addChild(this.BackGround);
        this.addChildAt(this.Lognmc, 1);
        this.Lognmc.play(10000);
        this.parent.touchChildren = false;
    }

    public Hide(): void
    {
        // 删除画面
        this.Lognmc.stop();
        this.removeChild(this.Lognmc);
        this.removeChild(this.BackGround);
        this.parent.touchChildren = true;
    }
}
