/**
 *
 * @author 
 *
 */
class CLoginUI extends eui.Component{
    public btnLogin: eui.Button;	    // 声明与ID相同名称的变量就可以动态绑定了
    public txtUsername: eui.TextInput;     // 声明与ID相同名称的变量就可以动态绑定了
    public txtPassword: eui.TextInput;     // 声明与ID相同名称的变量就可以动态绑定了

    public WaitingUI: CWaitingUI              // 等待UI画面

    constructor() {
        super();
        // 监听完成事件
        this.addEventListener(eui.UIEvent.COMPLETE,this.uiCompHandler,this);
        // 设置皮肤
        this.skinName = "resource/LoginUI/LoginUISkin.exml";

        // 获取保存的用户名
        this.txtUsername.text = egret.localStorage.getItem(Define.SAVE_DATA_USERNAME);
        this.txtPassword.text = egret.localStorage.getItem(Define.SAVE_DATA_PASSWORD);

        // 初始化等待界面
	    this.WaitingUI = new CWaitingUI();
        this.addChild(this.WaitingUI);
    }
    
    // UI初始话完成回调函数
    private uiCompHandler(): void {

         // 开启精灵动画
        this.StartSprites();

        // 添加按钮回调事件
        this.btnLogin.addEventListener(egret.TouchEvent.TOUCH_TAP,this.onButtonClick_Login,this);
    }

    // 启动其他动态元素的显示
    private StartSprites(): void
    {
        /*
        var Lognmc: egret.MovieClip;                // Logo动画
        var MCF: egret.MovieClipDataFactory;        // 动画工厂类

        // 获取资源
        var data = RES.getRes("Waiting_json");
        var tex = RES.getRes("Waiting_png");

        // 初始化动画工厂类
         MCF = new egret.MovieClipDataFactory(data,tex);

        // 从工厂类中获取指定动画
        Lognmc = new egret.MovieClip(MCF.generateMovieClipData("Waiting"));
        Lognmc.x = (800 - 100) / 2;
        Lognmc.x = (450 - 100) / 2;
        this.addChildAt(Lognmc, 1);
        Lognmc.play(10000);
        */
    }
    
    // 点击登录
    private onButtonClick_Login(e: egret.TouchEvent){
        egret.localStorage.setItem(Define.SAVE_DATA_USERNAME, this.txtUsername.text);
        egret.localStorage.setItem(Define.SAVE_DATA_PASSWORD, this.txtPassword.text);
        // 网络通讯
        var LoginPost:CMyPostSocket = new CMyPostSocket();
        LoginPost.addEventListener(CNetEvent.Event, this.onPostComplete, this);
        LoginPost.Connect("http://purpleshine.cn:8085/DifferentWorld/Login/toLogin");
        // LoginPost.Connect("http://localhost:8080/Login/toLogin");
        LoginPost.SendMsg("username="+this.txtUsername.text +"&password="+this.txtPassword.text, true);

       this.WaitingUI.Show();
    }

    // 登录回应事件
    private onPostComplete(event:CNetEvent){
        // 结束等待
        this.WaitingUI.Hide();

        if(event.Finish)
        {
            var panel:string;

            // 获取返回内容
            panel = event.Context.response.substring(0,50);

            if(panel=="yes"){
                // 页面跳转
                var SwitchEvent:CUISwitchEvent = new CUISwitchEvent(CUISwitchEvent.Event);
                SwitchEvent.From = "LoginUI";
                SwitchEvent.To = "MapUI";
                //发送要求事件
                this.dispatchEvent(SwitchEvent);
            }
            else{
                var messagebox = new CMessageBox();
                // this.stage.addChild(panel);
                messagebox.Show(this.stage, "提示", "测试用户名:a  密码:a", CMessageBox.ID_ClOSE);
            }
        }
        else{
            // 提示出错了
            var messagebox = new CMessageBox();
            // this.stage.addChild(panel);
            messagebox.Show(this.stage, "出错", "网络连接失败，请稍候尝试!", CMessageBox.ID_ClOSE);
        }
    }
}
