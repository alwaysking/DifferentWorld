// TypeScript file

class CShopUI extends egret.DisplayObjectContainer{
    public constructor(){
        super();
        this.addEventListener(egret.Event.ADDED_TO_STAGE,this.onAddToStage,this);
    }

    private onAddToStage(event:egret.Event){
       var button = new eui.Button();
        button.label = "Shop";
        button.horizontalCenter = 0;
	    button.verticalCenter = 0;
	    this.addChild(button);
    }
}