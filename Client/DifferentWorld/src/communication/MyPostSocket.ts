//////////////////////////////////////////////////////////////////////////////////////
//
//  Copyright (c) 2014-2015, Egret Technology Inc.
//  All rights reserved.
//  Redistribution and use in source and binary forms, with or without
//  modification, are permitted provided that the following conditions are met:
//
//     * Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright
//       notice, this list of conditions and the following disclaimer in the
//       documentation and/or other materials provided with the distribution.
//     * Neither the name of the Egret nor the
//       names of its contributors may be used to endorse or promote products
//       derived from this software without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY EGRET AND CONTRIBUTORS "AS IS" AND ANY EXPRESS
//  OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
//  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
//  IN NO EVENT SHALL EGRET AND CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
//  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
//  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;LOSS OF USE, DATA,
//  OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
//  LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
//  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
//  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
//////////////////////////////////////////////////////////////////////////////////////

class CMyPostSocket extends egret.Sprite {

    public request: egret.HttpRequest; 

    public constructor() {
        super();

        // 初始化Request
        this.request = new egret.HttpRequest;
        this.request.responseType = egret.HttpResponseType.TEXT;
    }

    // 连接
    public Connect(strUrl:string):void
    {        
        // 连接
        this.request.open(strUrl, egret.HttpMethod.POST);
        //设置响应头
        this.request.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
    }

    // 发送消息
    public SendMsg(strMsg:string, showUI:boolean):void
    {
        // 添加监听事件
        this.request.addEventListener(egret.Event.COMPLETE,this.onPostComplete,this);
        this.request.addEventListener(egret.IOErrorEvent.IO_ERROR,this.onPostIOError,this);

        //发送参数
        this.request.send(strMsg);
    }

    // 发送成功处理
    private onPostComplete(event: egret.Event) {
        var OkEvent:CNetEvent = new CNetEvent(CNetEvent.Event);
        OkEvent.Finish = true;
        OkEvent.Context = <egret.HttpRequest>event.currentTarget;
        //发送要求事件
        this.dispatchEvent(OkEvent);      
    }

    // 发送失败处理
    private onPostIOError(event: egret.IOErrorEvent): void {
        var OkEvent:CNetEvent = new CNetEvent(CNetEvent.Event);
        OkEvent.Finish = false;
        //发送要求事件
        this.dispatchEvent(OkEvent);  
        }
}
