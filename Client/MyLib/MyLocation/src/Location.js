// TypeScript file
var MyLocation;
(function(MyLocation){

    // 获取地理坐标
    function getLocation()
	{
        console.log("获取位置")
        if (navigator.geolocation){
            navigator.geolocation.getCurrentPosition(showPosition,showError);
        }
        else{console.log("出错了!")}
	}

    // 显示地理坐标
    function showPosition(position)
    {
        console.log("Latitude: " + position.coords.latitude + 
        "Longitude: " + position.coords.longitude);	
    }

    // 获取位置出错了
    function showError(error)
    {
        switch(error.code) 
        {
            case error.PERMISSION_DENIED:
            	console.log("User denied the request for Geolocation.");
            break;
            case error.POSITION_UNAVAILABLE:
            	console.log("Location information is unavailable.");
            break;
            case error.TIMEOUT:
            	console.log("The request to get user location timed out.");
            break;
            case error.UNKNOWN_ERROR:
            	console.log("An unknown error occurred.");
            break;
        }
    }
    function onClickEvent(label,duration){
                console.log(label + duration);
    }

    MyLocation.onClickEvent = onClickEvent;
    MyLocation.getLocation = getLocation;
})(MyLocation || (MyLocation={}));